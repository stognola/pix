const Area = require('./Area');
const Challenge = require('./Challenge');
const Competence = require('./Competence');
const Course = require('./Course');
const Skill = require('./Skill');
const Tutorial = require('./Tutorial');

module.exports = {
  Area,
  Challenge,
  Competence,
  Course,
  Skill,
  Tutorial,
};
