import { computed } from '@ember/object';
import Component from '@ember/component';
import _ from 'lodash';
import answersAsObject from 'mon-pix/utils/answers-as-object';
import solutionsAsObject from 'mon-pix/utils/solution-as-object';
import labelsAsObject from 'mon-pix/utils/labels-as-object';
import resultDetailsAsObject from 'mon-pix/utils/result-details-as-object';

function _computeAnswerOutcome(inputFieldValue, resultDetail) {
  if (inputFieldValue === '') {
    return 'empty';
  }
  return resultDetail === true ? 'ok' : 'ko';
}

function _computeInputClass(answerOutcome) {
  if (answerOutcome === 'empty') {
    return 'correction-qroc-box__input-no-answer';
  }
  if (answerOutcome === 'ok') {
    return 'correction-qroc-box__input-right-answer';
  }
  return 'correction-qroc-box__input-wrong-answer';
}

const QrfcmSolutionPanel = Component.extend({

  blocks:  computed('challenge.proposals', 'answer.value', 'solution', function() {
    const labels = labelsAsObject(this.get('challenge.proposals'));
    const solutions = solutionsAsObject(this.solution);
    const proposal = this.get('challenge.proposals');
    // const keys = Object.getOwnPropertyNames(solutions);
    const answers = answersAsObject(this.get('answer.value'), _.keys(labels));
    const chunks = proposal.split(/\$|\}/);
    const blocks = [];
    chunks.forEach((chunk) => {
      if (chunk.startsWith('{')) {
        const key = chunk.substr(1, chunk.indexOf(';') - 1);
        if (solutions[key][0] ===  answers[key]) {
          blocks.push({ good:solutions[key][0] });
        }
        else {
          blocks.push({ bad:answers[key], correctAnswer: solutions[key][0] });
        }
      }
      else {
        blocks.push({ text:chunk });
      }
    });
    return blocks;
  }),

  inputFields: computed('challenge.proposals', 'answer.value', 'solution', function() {

    const labels = labelsAsObject(this.get('challenge.proposals'));
    const answers = answersAsObject(this.get('answer.value'), _.keys(labels));
    const solutions = solutionsAsObject(this.solution);
    const resultDetails = resultDetailsAsObject(this.get('answer.resultDetails'));
    
    const inputFields = [];

    _.forEach(labels, (label, labelKeyAndChoices) => {
      const labelKey = labelKeyAndChoices.split(';')[0];
      const answerOutcome = _computeAnswerOutcome(answers[labelKey], resultDetails[labelKey]);
      const inputClass = _computeInputClass(answerOutcome);
      
      if (answers[labelKey] === '') {
        answers[labelKey] = 'Pas de réponse';
      }
      const inputField = {
        label: labels[labelKey],
        answer: answers[labelKey],
        solution: solutions[labelKey][0],
        emptyOrWrongAnswer: (answerOutcome === 'empty' || answerOutcome === 'ko'),
        inputClass
      };
      inputFields.push(inputField);
    });

    return inputFields;
  })

});

export default QrfcmSolutionPanel;

